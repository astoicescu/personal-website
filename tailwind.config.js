module.exports = {
  future: {
    /**
     * Remove this when Tailwind reaches v2.0, as instructed in
     * https://tailwindcss.com/docs/upcoming-changes#remove-deprecated-gap-utilities
     */
    removeDeprecatedGapUtilities: true,
  },
}

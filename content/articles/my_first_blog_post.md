---
title: First
description: Learn how to use @nuxt/content.
author:
  name: Benjamin
  bio: All about Benjamin
  image: https://source.unsplash.com/random
---

## A heading

Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.

## Another heading

It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.

## One more heading

It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

```js[a-filename.md]
const sayHah = () => {
  console.log('hah')
}
```

<div class="bg-blue-500 text-white p-4 mb-4">
  This is HTML inside markdown that has a class of note
</div>

<info-box>
  <template #info-box>
    This is a vue component inside markdown using slots
  </template>
</info-box>
